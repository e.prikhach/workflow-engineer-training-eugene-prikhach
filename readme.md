# Introducing into markdown

<p> Hello everyone coming here. I'm Eugene Prikhach, junior python dev. </p>
<p>During the time that I have been learning python, 
I have worked with a significant number of technologies,
from console applications to ml and cv.
I take part in kaggle competitions and help beginners learn the basics of python.
Took part in solving telegram data clustering contest.</p>

<p>Currently, in my spare time developing
a news web application using django
(and vue in perspective) and a mail window
application using pyqt and google api.</p>

<h3>My Skills Scoreboard</h3>

| Technology | rate |
| ------------- | ------------- |
| Python  | 5  |
| MySQL  | 4 |
| CSV  | 5  |
| YAML  | 2 |
| RedMine  | 1 |
| Jira  | 1 |

<p>I work every day from 
10 to 10 (sometimes later :)),
so if you have any questions about tasks, write,
I will help you in any way I can :)</p>

You can contact me at any link below :) <br>

<h3>[VK](https://vk.com/r1khen) <br>
[Jira](https://workflow-engineers-syberry.atlassian.net/jira/people/604ba948ad0d2e00686eb947) <br>
[GiHub](https://github.com/eprikhach) <br>
[GitLab](https://gitlab.com/e.prikhach) <br></h3>

